FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

ARG VERSION=1.2.12

RUN mkdir -p /app/code /app/data
WORKDIR /app/code

# install cloud-torrent
RUN curl --fail -# -L https://github.com/boypt/simple-torrent/releases/download/$VERSION/cloud-torrent_linux_amd64.gz \
	| gzip -d - > /usr/local/bin/cloud-torrent && \
	chmod +x /usr/local/bin/cloud-torrent

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
