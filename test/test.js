#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

const LOCATION = 'test';
const TIMEOUT = 100000;
const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

describe('Application life cycle test', function () {
    this.timeout(0);

    var browser;
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    }

    const TORRENT_URL_INPUT_XPATH = '/html/body/div/section[2]/div[2]/input';
    const TORRENT_URL = 'magnet:?xt=urn:btih:b674f2afa42d2b72b5d5dbb6965d23edaebb2364&dn=archlinux-2018.10.01-x86_64.iso&tr=udp://tracker.archlinux.org:6969&tr=http://tracker.archlinux.org:6969/announce';
    const TORRENT_FILE_NAME = 'archlinux-2018.10.01-x86_64.iso';

    function login(done) {
        browser.get(`https://${app.fqdn}/login`).then(function () {
            return browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.tagName('form')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath(TORRENT_URL_INPUT_XPATH)), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function logout(done) {
        browser.get(`https://${app.fqdn}/logout`).then(function () {
            return browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }
 
    function loadTorrent(done) {
        browser.get(`https://${app.fqdn}`).then(function () {
            return browser.wait(until.elementLocated(By.xpath(TORRENT_URL_INPUT_XPATH)), TIMEOUT);
        }).then(function () {
            return browser.findElement(By.xpath(TORRENT_URL_INPUT_XPATH)).sendKeys(TORRENT_URL);
        }).then(function () {
            // give the input validator some time
            return browser.sleep(2000);
        }).then(function () {
            return browser.findElement(By.xpath(TORRENT_URL_INPUT_XPATH)).sendKeys(Key.ENTER);
        }).then(function () {
            // give the angular app some time to load the torrent
            return browser.sleep(5000);
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath(`//*[text()="${TORRENT_FILE_NAME}"]`)), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function isDownloading(done) {
        browser.get(`https://${app.fqdn}`).then(function () {
            // give the angular app some time to load the torrent
            return browser.sleep(5000);
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath('//h5[@ng-click="section_expanded_toggle()"]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(By.xpath('//h5[@ng-click="section_expanded_toggle()"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath(`//*[text()="${TORRENT_FILE_NAME}"]`)), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can load torrent', loadTorrent);
    it('can logout', logout);
    it('can restart app', function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });
    it('can login', login);
    it('still downloading', isDownloading);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('still downloading', isDownloading);
    it('can logout', logout);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);

            var inspect = JSON.parse(execSync('cloudron inspect'));
            app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
            expect(app).to.be.an('object');

            done();
        });
    });

    it('can login', login);
    it('still downloading', isDownloading);
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('can install app', function () {
        execSync(`cloudron install --appstore-id com.github.cloudtorrent --location ${LOCATION}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can load torrent', loadTorrent);
    it('can logout', logout);

    it('can update', function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('is still downloading', isDownloading);
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
});
