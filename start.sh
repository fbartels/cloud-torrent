#!/bin/bash

set -eu

chown -R cloudron:cloudron /app/data

# https://github.com/boypt/simple-torrent/wiki/Config-File
# this will automatically create /app/data/cloud-torrent.yaml on first run and download to /app/data/downloads
echo "=> Starting cloud torrent"
cd /app/data
exec /usr/local/bin/gosu cloudron:cloudron /usr/local/bin/cloud-torrent
